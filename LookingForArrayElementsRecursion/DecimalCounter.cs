﻿using System;

#pragma warning disable S2368

namespace LookingForArrayElementsRecursion
{
    public static class DecimalCounter
    {
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (CheckRanges(ranges) == 2)
            {
                throw new ArgumentException("The length of one of the ranges is less or greater than 2.");
            }

            if (CheckRanges(ranges) == -1)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            return GetDecimalsCount(arrayToSearch[1..], ranges) + GetOneDecimalCount(ranges, arrayToSearch[0]);
        }

        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (CheckRanges(ranges) == 2)
            {
                throw new ArgumentException("The length of one of the ranges is less or greater than 2.");
            }

            if (CheckRanges(ranges) == -1)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (startIndex < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (count == 0)
            {
                return 0;
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            decimal[] trimmedArray = arrayToSearch[startIndex .. (startIndex + count)];

            return GetDecimalsCount(arrayToSearch, ranges, startIndex + 1, count - 1) + GetOneDecimalCount(ranges, trimmedArray[0]);
        }

        private static int GetOneDecimalCount(decimal[][] ranges, decimal value)
        {
            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            if (ranges[0].Length != 2)
            {
                return 0;
            }

            if (value >= ranges[0][0] && value <= ranges[0][1])
            {
                return 1;
            }

            return GetOneDecimalCount(ranges[1..], value);
        }

        private static int CheckRanges(decimal[][] ranges)
        {
            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            if (ranges[0] is null)
            {
                return -1;
            }

            if (ranges[0].Length > 0 && ranges[0].Length != 2)
            {
                return 2;
            }

            return CheckRanges(ranges[1..]);
        }
    }
}
