﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class IntegersCounter
    {
        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor));
            }

            if (elementsToSearchFor.Length == 0)
            {
                return 0;
            }

            return GetIntegersCount(arrayToSearch, elementsToSearchFor[1..]) + GetOneIntegerCount(arrayToSearch, elementsToSearchFor[0]);
        }

        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor));
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (startIndex > arrayToSearch.Length || startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (elementsToSearchFor.Length == 0)
            {
                return 0;
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            int[] trimmedArray = arrayToSearch[startIndex .. (startIndex + count)];

            return GetIntegersCount(arrayToSearch, elementsToSearchFor[1..], startIndex, count) + GetOneIntegerCount(trimmedArray, elementsToSearchFor[0]);
        }

        private static int GetOneIntegerCount(int[] arrayToSearch, int value)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (arrayToSearch[0] == value)
            {
                return GetOneIntegerCount(arrayToSearch[1..], value) + 1;
            }

            return GetOneIntegerCount(arrayToSearch[1..], value);
        }
    }
}
