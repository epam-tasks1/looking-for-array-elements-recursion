﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class FloatCounter
    {
        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart));
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd));
            }

            if (rangeStart.Length == 0 || rangeEnd.Length == 0)
            {
                return 0;
            }

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException("Arrays of range starts and range ends contain different number of elements.");
            }

            if (rangeStart[0] > rangeEnd[0])
            {
                throw new ArgumentException("The range start value is greater than the range end value.");
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            return GetFloatsCount(arrayToSearch, rangeStart[1..], rangeEnd[1..]) + GetOneFloatCount(arrayToSearch, rangeStart[0], rangeEnd[0]);
        }

        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (rangeStart is null)
            {
                throw new ArgumentNullException(nameof(rangeStart));
            }

            if (rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(rangeEnd));
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (rangeStart.Length == 0 || rangeEnd.Length == 0)
            {
                return 0;
            }

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException("Arrays of range starts and range ends contain different number of elements.");
            }

            if (rangeStart[0] > rangeEnd[0])
            {
                throw new ArgumentException("The range start value is greater than the range end value.");
            }

            if (startIndex + count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            float[] trimmedArray = arrayToSearch[startIndex .. (startIndex + count)];

            return GetFloatsCount(arrayToSearch, rangeStart[1..], rangeEnd[1..], startIndex, count) + GetOneFloatCount(trimmedArray, rangeStart[0], rangeEnd[0]);
        }

        private static int GetOneFloatCount(float[] arrayToSearch, float start, float end)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (arrayToSearch[0] >= start && arrayToSearch[0] <= end)
            {
                return GetOneFloatCount(arrayToSearch[1..], start, end) + 1;
            }

            return GetOneFloatCount(arrayToSearch[1..], start, end);
        }
    }
}
